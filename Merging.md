[Home](Readme.md)
## Merging objects into atlas 
This tutorial covers process of merging two objects into atlas. On a screenshot, there is two models that were made by [Fabianvan Dorst](https://sketchfab.com/FabianvanDorst). Models are using 4 common texture maps: Base Color, Height Map, Normal and Roughness.  
  
![image](Images/merging.png) 

And total count of textures is 8. The goal is to merge BaseColor for cone and Tire into one file. And do that for the rest of other maps the same way. As a result, there will be 4 merged maps with same purpose. Every merged map will contain two resized textures: for cone and for tire.   

#### 1. Create New Project  
Press "New Project" button. Enter BaseColor, Normal, Height and Roughness texture names in OutputTextures field. Set target atlas size in pixels to 4096 or any other sane value. Set Aspect to 1:1. This value means that square atlas will be generated. Select desired output format for textures in Output Format filed. Press "OK". 
  
![image](Images/NewProject.png)   

#### 2. Setup destination folder and desired prefix for merged atlas  
In a properties panel from a right, select Destination Folder path and specify DestinationObjectName. This name will be used as prefix for generated maps. For example, if DestinationObjectName is "merged" the BaseColor atlas will be named "merged_BaseColor.tga" for that case. You can change these settings at any time by editing atlas properties in properties panel. To view atlas properties, click on empty gray-colored space.  

#### 3. Import textures in project  
Press "Create Item from Set of Textures" button. Locate folder with textures for a tire, and select all 4 files. New atlas item with name Tire01_low_1001_BaseColor will be generated. All textures in Textures property of newly generated item will be filled from selected files in alphabet order. There is also option to import every texture in manually. Press "Create New Item" button to create empty item. The space in atlas is automatically subdivided to contain two elements. Subdividing will occurs if atlas doesn't have enough space for new element. The next two elements won't require any subdivisions. Select newly created element and select required textures in Textures rollout manually. As soon as BaseColor will be selected preview of the element will be updated. Set ObjectName parameter to "Cone". You can delete atlas item by pressing delete key on keyboard.    

#### 4. Check what is inside evert texture output  
Click on empty space to see atlas properties. Select texture to inspect in "PreviewTextureNameField" The project view will be updated to display selected output texture.

#### 5. Tweak Size
Select Cone item. Locate "Size" Group in properties and change Width and Height to 2. This makes Cone two times larger than tire. However, items doesn't fit into atlas now. Press "Repack" button to pack items inside atlas. 
  
![image](Images/Packed.png) 

#### 6. Bake maps  
Press "Bake" button. After baking will be completed a destination folder containing 5 baked files will be opened.   
  
![image](Images/baked_files.png)

#### 7. Remap models in 3ds Max  
Open 3ds Max and run script "UVToAltlas.ms" that can be located inside AtlasPacker installation folder. Import cone model into scene. Press "Browse" and locate merged.xml. With cone model selected, select Cone element inside script and press "Remap" button. Create new standard material and set baked maps to check the result. Cone is now remapped into baked texture. Do same steps for tires. Assign same material for both objects.  
  
![image](Images/remap.png)   

#### 8. Save your work  
Press "Save" button to save project  

![image](Images/merged_uvs.png)  
![image](Images/merged_objects.png)  
  
