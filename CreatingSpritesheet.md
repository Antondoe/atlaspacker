[Home](Readme.md)
## Creating Animation Flipbook  
This tutorial covers a process of creating a 4x2 uniform flipbook from existing 8 animation frames. 
  
#### 1. Create New Project  
Press "New Project" button. Set "BaseColor" only value in OutputTextures field. Specify desired resolution, aspect and output format. Press "OK".  

#### 2. Setup destination folder and desired prefix for merged atlas  
In a properties panel from a right, select Destination Folder path and specify DestinationObjectName. This name will be used as prefix for generated maps. For example, if DestinationObjectName is "merged" the BaseColor atlas will be named "merged_BaseColor.tga" for that case. You can change these settings at any time by editing atlas properties in properties panel. To view atlas properties click on empty gray-colored space.  

#### 3. Import animation frames  
Select sprites in a folder an drop them into atlas pasker to create new items. This although can be done by pressing "Create Item from Set of Textures" button. The program will create 8 atlas elements and automatically repack them so they will fit into square. New items will have same names as selected textures and keep the same order as they have in selection. (for example, files may be sorted by alphabetical order or by date) It's good to sort files in alphabetical order so that they will follow each as frames in animation.  
  
![image](Images/sprites.png) 

#### 4. Adjust size and position  
For that tutorial, non-quad sprites were used. Select all imported sprites and set their proper size. 
  
![image](Images/sprties_size.png)  
Sprites are overlapped now. Press repack to find optimal layout.  
  
![image](Images/sprties_repack.png)  
Adjust sprites position if it is necessary.

#### 4. Bake  
Press "Bake" button. After baking will be completed a destination folder containing baked spritesheet.  

#### 5. Save your work  
Press "Save" button to save project
