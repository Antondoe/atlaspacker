# Atlas Packer
![image](Images/screen.png)
### What is this 
Atlas packer works with object UVs and textures at same time. The tool can be used to change a set of objects, that use separate texture files, so that they will be  use one shared texture usually called atlas. It also can be used to produce spritesheets or generate any kind of atlases for 2D games. For example, it can merger separate animation frames into one sprtitesheet (or flipbook) image in two clicks. When it comes to optimization, many renderers are using batching to increase rendering. This method groups objects by material and render them with minimum context switching operations. Merging objects into one atlas helps to decreasae material count in scene and increase rendering speed.  
![image](Images/merging.png) 

### How it works 
The program uses ImageMagick lib to perform operations over textures. It requires 3ds max to run script that transforms uv sets for models. After atlas is developed in Atlas Packer it may be exported as a set of textures. When exporting, the program generates additional xml file that provides information about UV transforms for corresponding objects. This file is used by scrip that comes with the package. To apply changes to UVs for object, open 3ds Max and launch script UVToAltlas.ms and locate exported xml file by pressing "Browse" button. Import object which UVs need to be changed to be map into atlas. Select in list appropriate atlas element in which object should be mapped and press "Remap" button.

*****

# Downloads  
[Win64 Archive](https://bitbucket.org/Antondoe/atlaspacker/downloads/Release.zip)


*****

# Features  
* Saving. Save project to make some changes later
* Multi-map loading. Select and load all required textures in one click
* Multi-map processing. Import full set of textures that are used by models and process them all together
* Non-quad atlas and textures. All aspects are supported
* Rotation. Rotate elements to best fit
* Auto packing. Set weights for textures and pack them in optimal way
* Manual repacking. Lay out all textures in manual mode using snapping
* Lock mode to prevent some items from changing their positions due to auto-repacking
* Increase / Decrease size operation for items with specified aspect
* Import / Export from png, tga, tiff, jpg and many other formats
* Drag and Drop. Drag textures into app to create new atlas elements  

### Multi-map loading  
This feature loads all selected textures and automatically groups them into atlas elements. For example, Atlas is configured to bake two output textures: Diffuse and Normal. There are two objects A and B with following texture sets stored in one folder: A_diffuse.png, A_normal.png, B_diffuse.png, B_normal.png. Those files may be loaded all at once by pressing "Create Item from Set of Textures". In a given case, every two files from a set will  be treated as single new atlas item with a name of first texture in a pair. As a result, two items will be added to atlas: A_Diffuse and B_Diffuse. A_Diffuse item will have A_diffuse in Diffuse texture and A_normal in normal texture. B_Diffuse will have B_diffuse in diffuse and B_normal in Normal.

### Automatic repacking   
Allows to layout all items in atlas automatically. Every item has its size in atlas. This is an integer number that controls how much space is occupied by item in relation to other items. For example, if size of A is 4x4 and size of B is 2x2, then A occupies two times more space than B. When repacking, atlas is subdivided into cells. If item A have a size of 4x4 this means that it fills 16 cells in atlas. The number of cells is always a minimal number that is necessary to pack all items in optimal way, respecting to atlas aspect.  

### Manual repacking   
Drag and drop items to control their position. Items are snapped to grid cells. Use Lock optoin to freeze some items at their positions when auto repacking is applied.

*****  

# Understanding packing  
Atlas packer allows to pack items in different ways. Packing can be performed over all items or over selection. In second case, selected items will be collected together and placed at free position. Packed items won't affect other items. There are two different packing modes now. Modes can be switched by use uniform packing checkbox. 
  
![image](Images/packing_options.png) 

### Control size of items
The size options allows to choose how to handle items with different size during packing. There is an options to keep size as it is, or make all items sizes equals to size of largest or smallest item. 
  
### Sort items
Packer can sort items from left to right and up to down order by using item name or size. Order can be reversed by setting up inverse order checkbox. 

### Uniform packing
In this mode packer spreads all items over a grid with specified amount of cols and rows. One cell of grid contains one item. All cells will have same size, calculated as max size of all selected elements. Size of cell affects on aspect of grid. If grid size is 3x3 and max size of element is 1x2 than packed items will consume 3x6 grid cells. There is e few options to control how to handle items size during packing process.  
  
![image](Images/repack_1.png) 
![image](Images/repack_2.png) 

### Optimal packing
This mode packs items into rectangle with aspect specified in rows and cols parameters. So, packing items in this mode with sizes 1x1 and 3x3 will give same result. In this mode, packer tries to find optimal rectangle layout for all selected items to fit into minimal rectangle area with specified aspect.  
  
![image](Images/repack_3.png) 
   
*****

# Understanding baking
### Output Textures   
A set of output textures can be configured by any time in atlas properties. AtlasPacker is using information provided in atlas settings to display texture slots for atlas items. If some output texture will be renamed, corresponding texture slots for all items will show a new name, however, the content of slots won't be changed. If new output texture will be created, then new slot for all items will be added. If some item will be removed, then corresponding slots will be removed for items.  
  
Every target texture has two parameters:  

Parameter         | Description
----------------- | --------------------------------------------------------------
Name              | Name is used to identify slot in list of textures
BackgroundColor   | This color is used as a background for whole output texture  

When new atlas item being created it inherits BackgroudColor for texture slots from atlas settings. BackgroundColor for slot can be overriden for every atlas element.  

### Baking   
It's necessary to understand that colors and alpha are processed separately. First, a master texture with target size is created. The texture uses BackgroundColor from atlas settings as background.   
![image](Images/Processing_items.png)  
BackgroundColor of slot in atlas item, will be mixed with background of a texture. **It doesn't affect texture's alpha**, however in **current version of AtlasPacker items with non-transparent background are rendered as they have no alpha**. This technique may be used to prevent color leaks that may happens when resizing. In tga file format Alpha is stored separately from RGB in Alpha channel as clipping mask. When size of image is changed, alpha may clip less pixels than before and some parts of background may become visible. This is absolutely normal situation. To handle this case, background color is usually picked as average color of image, so even if some pixels became visible, it still looks correct. **If there is no need to change background color of texture keep BackgroundColor value transparent**. If no texture is selected for a slot, then only slot's BackgroundColor will be baked. **It gives an option to bake some areas of texture with specific color.** 

After item texture is processed, it is composed with main texture. RGB values of texture are **replaced** on RGB values of item texture. The same will be done with alpha. Its right to say that BackgroundColor in atlas settings is used for areas of master texture that has no items.  

At the final step, RGB is mixed with mask as it is required by target file format. 

*****

# Tutorials  
[Merging objects](Merging.md) Covers how to merge objects into one atlas  
[Creating animation sprtiesheet](CreatingSpritesheet.md) Explains how to use Atlas Packer for managing sprites  


*****







 
 









